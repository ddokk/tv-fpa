
![](https://gitlab.com/ddokk/gfx/-/raw/main/g/b1.png)
<img src="./img/mm.gif" align="center">

----

<h1 align="center"><code> tv-fpa </code></h1>
<h2 align="center"><i> Illustration - Time Window -  hour of granting wishes every Friday (prayer)   </i></h2>

----
1. [Wat Do ?](#wat-do-)
2. [Standard Prayer Timings for your location](#standard-prayer-timings-for-your-location)
   1. [Application for you location](#application-for-you-location)
3. [Hadith References](#hadith-references)
   1. [Ref 1: `Sahih al-Bukhari 935`](#ref-1-sahih-al-bukhari-935)
   2. [Ref 2: `Sunan an-Nasa'i 1389`](#ref-2-sunan-an-nasai-1389)
   3. [`IslamQA` - Hour of Response](#islamqa---hour-of-response)
4. [Time Calculation](#time-calculation)
   1. [Prayer timings during the making of this repo](#prayer-timings-during-the-making-of-this-repo)
   2. [Time Span between Asr and Magrib (Saudi Arabia Timings)](#time-span-between-asr-and-magrib-saudi-arabia-timings)
   3. [Time Span Divisions](#time-span-divisions)
      1. [Time Calculations](#time-calculations)
5. [Calculated Time Window](#calculated-time-window)
6. [Illustration](#illustration)
   1. [Illustration Assets](#illustration-assets)
7. [Moon Phases Tracker](#moon-phases-tracker)

----

# Wat Do ?

1. This repo is an explanation of the special hour of Friday, in wich all duas are accepted
2. This repo will have its illustration which has been done with [`excalidraw`](https://excalidraw.com/)

# Standard Prayer Timings for your location 

[`Salah.com`](https://salah.com/)
- From the makers of [`https://quran.com/`](https://quran.com/)

## Application for you location 

1. Read this paper and make the calculations based on the timings of your own location. 

# Hadith References 

## Ref 1: `Sahih al-Bukhari 935`

[`(37)Chapter: An hour (opportune-lucky time) on Friday`](https://sunnah.com/bukhari:935)
> Allah's Messenger (ﷺ) (p.b.u.h) talked about Friday and said, "There is an hour (opportune time) on Friday and if a Muslim gets it while praying and asks something from Allah, then Allah will definitely meet his demand." And he (the Prophet) pointed out the shortness of that time with his hands.

## Ref 2: `Sunan an-Nasa'i 1389`
[`(14)Chapter: The Time Of Jumu'ah`](https://sunnah.com/nasai:1389) 
> The Messenger of Allah (ﷺ) said: "Friday is twelve hours in which there is no Muslim slave who asks Allah (SWT) for something but He will give it to him, so seek it in the last hour after 'Asr."

## [`IslamQA` - Hour of Response](https://islamqa.info/en/answers/201556/how-can-the-muslims-seek-the-hour-of-response-on-friday) 

> The first view is that it is from the time when the imam sits down until the end of the prayer, and the second is that it is after *‘Asr. The latter is the more correct of the two views.*

# Time Calculation 

1. The calculations are based on [`prayer`](https://www.islamicfinder.org/world/saudi-arabia/104515/makkah-prayer-times/) timings in [`Makkah, Saudi Arabia`](https://goo.gl/maps/GggNNLCrYaxsryrh7)
2. These timing caluclations were done on `Sat 01 Apr 2023 03:40:12 PM UTC`

## Prayer timings during the making of this repo 

Prayer | Timings (24hrs)
|:--:|:--:|
Asr | 15:31
Magrib | 18:36 

## Time Span between Asr and Magrib (Saudi Arabia Timings)

Calculating the [`time span`](https://www.wolframalpha.com/input?i=15%3A31+Saudi+Arabia+-+18%3A36+Saudi+Arabia+)

```ml 
3.083 hours
185 Minutes
3 Hours and 5 minutes
```
## Time Span Divisions 

1. Based on the above mentioned sources that hints at the `last hour after asr`
2. We will divide the time span by 3 
3. According to the above calculation, during the time this repo was written the time span is already `3.083 hours` 

### Time Calculations

Calculating the times 
- We will keep adding 1 hour from the time of `asr` until magrib

Asr | Adding One hour 
|:--:|:--:|
`15:31` | +1hr = 16:31
`16:31` | +1hr = 17:31
`17:31` | +1hr = 18:31

> The last hour falls between 17:31 & 18:36 

$$\Huge \color{green}{The \ last \ hour \ then \ falls \ between \ -17:31 \ - \ 18:31}$$

# Calculated Time Window 

> Between 17:31 - 18:31 

# Illustration 

[![](./img/tvf.png)](https://excalidraw.com/#json=r-DKWEdqHG4f1C0MByuOn,WpjM9rgj3dt_MYNQeddUzQ)

## Illustration Assets

_You can import this diagram for your won usage and further edits, usage is governed by [`LICENSE`](./LICENSE) mentioned in this repo_

Asset | Source 
|:--:|:--:|
URL to import diagram | [`https://excalidraw.com/#json=r-DKWEdqHG4f1C0MByuOn,WpjM9rgj3dt_MYNQeddUzQ`](https://excalidraw.com/#json=r-DKWEdqHG4f1C0MByuOn,WpjM9rgj3dt_MYNQeddUzQ)
JSON | [`Excalidraw File`](./img/tvfpa-2023-04-01-2020.excalidraw) <br> Import directly into [excalidraw](https://excalidraw.com/) for edits

# Moon Phases Tracker 

Collection on moon phase trackers from known sources

WebApp | URL 
|:--:|:--:|
Nasa official moon phases tracker | [`https://moon.nasa.gov/moon-in-motion/moon-phases/`](https://moon.nasa.gov/moon-in-motion/moon-phases/) <br> Used for putting the right moon phase emoji
Moon Giant | [`https://www.moongiant.com/phase/today/`](https://www.moongiant.com/phase/today/) <br> Informative site generally for moon phases